# Installing WSL Ubuntu on Windows 

## Follow the steps- 
1. open Windows Command Prompt in administration mode 
2. type ``` wsl --install ```
- Windows Subsystem for Linux will be installed and Ubuntu
3. Re-start the computer 
4. System will ask you for "Enter new UNIX username: " and than Password 

---

## Set up WSL in VScode
1. Open VS code
2. Install WSL from the VScode extention 
3. Press F1 
4. Select "Remote-WSL: New Window" 
5. open VScode Terminal, terminal>-new terminal  
6. Start by creating a folder ``` mkdir venv``` where all the .bash will be saved 

---
	
## Or 
1. open comand prompt 
2. type ```wsl```
3. type ``` code .```
- which will open in VScode
